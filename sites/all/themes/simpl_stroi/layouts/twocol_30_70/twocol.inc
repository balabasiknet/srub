<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 30/70'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol.png',
  'theme' => 'panels_twocol_30_70',
  'css' => 'twocol.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
